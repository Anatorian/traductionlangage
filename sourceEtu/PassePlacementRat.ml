(* Module de la passe de gestion du placement mémoire *)
module PassePlacementRat : Passe.Passe with type t1 = Ast.AstType.programme and type t2 = Ast.AstPlacement.programme =
struct

  open Tds
  open Ast
  open Type

  type t1 = Ast.AstType.programme
  type t2 = Ast.AstPlacement.programme


(* analyse_placement_instruction : AstType.instruction -> AstPlacement.instruction *)
(* Paramètre instruction : l'instruction à analyser *)
(* Paramètre deplacement : le déplacement courant *)
(* Paramètre registre : le registre auquel le déplacement réfère*)
(* Analyse les instruction afin de placer les variables en mémoire *)
let rec analyse_placement_instruction  instruction deplacement registre =
  match instruction with
  | AstType.Declaration(_, info) ->
    begin
      match info_ast_to_info info with
      | InfoVar(_, typ, _, _) -> 
        let _ = modifier_adresse_info deplacement registre info in
          getTaille typ
      | _ -> failwith "Internal error"
    end
  | AstType.TantQue(_, bloc) ->
    let _ = analyse_placement_bloc bloc deplacement registre in
      0
  | AstType.Conditionnelle(_, blocSi, blocSinon) ->
    let _ = analyse_placement_bloc blocSi deplacement registre in
      let _ = analyse_placement_bloc blocSinon deplacement registre in
        0
  | _ -> 0
      
(* analyse_placement_bloc : AstType.bloc -> AstPlacement.bloc *)
(* Paramètre li : liste d'instructions à analyser *)
(* Paramètre deplacement : le déplacement courant *)
(* Paramètre registre : le registre auquel le déplacement réfère*)
(* Parcours de la liste des instruction en ajoutant accumulant *)
(* la taille à chaque appel*)
and analyse_placement_bloc  li deplacement registre =
  let rec analyse_placement acc li =
    match li with
    | hd::tl -> 
      let tailleInstruction = analyse_placement_instruction hd (deplacement + acc) registre in
        analyse_placement (acc + tailleInstruction) tl
    | [] -> acc
  in
  let _ = analyse_placement 0 li in
    li


(* analyse_placement_fonction : AstType.fonction -> AstPlacement.fonction *)
(* Paramètre : la fonction à analyser *)
(* Défini le placement mémoire des variables et fonctions du programmes *)
let analyse_placement_fonction (AstType.Fonction(name,lparam,linstr,exprRetour))  =
  let _ = List.fold_right(fun infoParam acc ->
    match info_ast_to_info infoParam with
    | InfoVar(_, typ, _, _) -> 
      let _ = modifier_adresse_info (acc - getTaille typ) "LB" infoParam in
        (acc - getTaille typ)
    | _ -> failwith "Internal error") lparam 0 in
    let _ = analyse_placement_bloc linstr 3 "LB" in
      AstPlacement.Fonction(name, lparam, linstr, exprRetour)


(* analyser : AstType.ast -> AstPlacement.ast *)
(* Paramètre : le programme à analyser *)
(* Défini le placement mémoire des variables et fonctions du programmes *)
let analyser (AstType.Programme (fonctions,prog)) =
  let nf = List.map (analyse_placement_fonction) fonctions in 
  let nb = analyse_placement_bloc prog 0 "SB" in
  AstPlacement.Programme (nf,nb)

end

