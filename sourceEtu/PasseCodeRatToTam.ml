(* Module de la passe de traduction du code *)
module PasseCodeRatToTam : Passe.Passe with type t1 = Ast.AstPlacement.programme and type t2 = string =
struct

  open Tds
  open Ast
  open Type
  open Code

  type t1 = Ast.AstPlacement.programme
  type t2 = string

(* traduction_expression : AstPlacement.expression -> string *)
(* Paramètre e : l'expression à analyser *)
(* Renvoi le code TAM correspondant à l'expression e *)
let rec traduction_expression e = 
  match e with
  | AstType.AppelFonction(infoAst, exprList) -> 
    begin
      match info_ast_to_info infoAst with
      | InfoFun(name, _, _) ->
        let tradExpr = List.fold_left(fun acc elt -> acc ^ traduction_expression elt) "" exprList in
        let call = "CALL (SB) " ^ name ^ "\n" in
        tradExpr ^ call 
      | _ -> failwith "Internal error"
    end
  | AstType.Rationnel(expr1, expr2) -> 
    let c1 = traduction_expression expr1 in
    let c2 = traduction_expression expr2 in
    c1 ^ c2
  | AstType.Numerateur(expr) -> traduction_expression expr ^ "POP (0) 1\n"
  | AstType.Denominateur(expr) -> traduction_expression expr ^ "POP (1) 1\n"
  | AstType.Ident(infoAst) -> 
    begin
      match info_ast_to_info infoAst with
      | InfoVar(_, typ, depl, base) -> 
        let tailleStr = string_of_int (getTaille typ) in
        "LOAD (" ^ tailleStr ^ ") " ^ string_of_int depl ^ "[" ^ base ^ "]\n"
      | InfoConst(_, value) -> "LOADL " ^ string_of_int value ^ "\n"
      | _ -> failwith "Internal error"
    end
  | AstType.True -> "LOADL 1\n"
  | AstType.False -> "LOADL 0\n"
  | AstType.Entier(entier) -> "LOADL " ^ string_of_int entier ^ "\n"
  | AstType.Binaire(operande, expr1, expr2) -> 
    match operande with
    | PlusInt -> 
      let c1 = traduction_expression expr1 in
      let c2 = traduction_expression expr2 in
      c1 ^ c2 ^ "SUBR IAdd\n"
    | PlusRat ->
      let c1 = traduction_expression expr1 in
      let c2 = traduction_expression expr2 in
      c1 ^ c2 ^ "CALL (SB) RAdd\n"
    | MultInt ->
      let c1 = traduction_expression expr1 in
      let c2 = traduction_expression expr2 in
      c1 ^ c2 ^ "SUBR IMul\n"
    | MultRat ->
      let c1 = traduction_expression expr1 in
      let c2 = traduction_expression expr2 in
      c1 ^ c2 ^ "CALL (SB) RMul\n"
    | EquInt ->
      let c1 = traduction_expression expr1 in
      let c2 = traduction_expression expr2 in
      c1 ^ c2 ^ "SUBR IEq\n"
    | EquBool ->
      let c1 = traduction_expression expr1 in
      let c2 = traduction_expression expr2 in
      c1 ^ "SUBR B2I\n" ^ c2 ^ "SUBR B2I\n" ^ "SUBR IEq\n"
    | Inf ->
      let c1 = traduction_expression expr1 in
      let c2 = traduction_expression expr2 in
      c1 ^ c2 ^ "SUBR ILss\n"

(* traduction_instruction : AstPlacement.instruction -> string *)
(* Paramètre instruction : l'instruction à analyser *)
(* Renvoi le code TAM correspondant à l'instruction *)
let rec traduction_instruction  instruction =
  (* Penser aux retour à la ligne *)
  match instruction with
  | AstType.Declaration(expr, infoAst) -> 
    begin 
      match info_ast_to_info infoAst with
      | InfoConst(_) -> failwith "Internal Error"
      | InfoVar(_, typ, deplacement, base) ->
        (* Réserver la place *)
        (* Analyser l'expression *)
        (* STORE (taille) deplacement[base] *)
        let tailleStr = string_of_int (getTaille typ) in
        let push = "PUSH " ^ tailleStr ^ "\n" in
        let c1 = traduction_expression expr in
        (getTaille typ ,push ^ c1 ^ "STORE (" ^ tailleStr ^ ") " ^ string_of_int deplacement ^ "[" ^ base ^ "]\n")
      | InfoFun(_) -> failwith "Internal error"
    end
  | AstType.Affectation(expr, info_ast) -> 
    (* Analyse expression *)
    (* STORE (taille) deplacement[base] *)
    let c1 = traduction_expression expr in
    begin
      match info_ast_to_info info_ast with
      | InfoVar(_, typ, deplacement, base) ->
        let tailleStr = string_of_int (getTaille typ) in
        let store = "STORE (" ^ tailleStr ^ ") " ^ string_of_int deplacement ^ "[" ^ base ^ "]\n" in
        (0, c1 ^ store)
      | _ -> failwith "Internal Error"
    end
  | AstType.AffichageInt(expr) -> 
    let strExpr = traduction_expression expr in
      (0, strExpr ^ "SUBR IOut\n")
  | AstType.AffichageRat(expr) -> 
    let strExpr = traduction_expression expr in
      (0, strExpr ^ "CALL (SB) Norm\n" ^ "CALL (SB) ROut\n")
  | AstType.AffichageBool(expr) -> 
    let strExpr = traduction_expression expr in
      (0, strExpr ^ "SUBR BOut\n")
  | AstType.Conditionnelle(expr, blocif, blocelse) -> 
    let c1 = traduction_expression expr in
    let etiquetteFin = getEtiquette() in
    let etiquetteElse = getEtiquette() in
    let jumpElse = "JUMPIF (0) " ^ etiquetteElse ^ "\n" in
    let jumpFin = "JUMP " ^ etiquetteFin ^ "\n" in
    let phrase = c1 ^ jumpElse ^ traduction_bloc blocif ^ jumpFin ^ etiquetteElse ^ "\n" ^ traduction_bloc blocelse ^ etiquetteFin ^ "\n" in
    (0, phrase)
  | AstType.TantQue(expr, bloc) ->
    let c1 = traduction_expression expr in
    let etiquetteFin = getEtiquette() in
    let etiquetteDebut = getEtiquette() in
    let jumpFin = "JUMPIF (0) " ^ etiquetteFin ^ "\n" in
    let jumpDebut = "JUMP " ^ etiquetteDebut ^ "\n" in
    let phrase = etiquetteDebut ^ "\n" ^ c1 ^ jumpFin ^ traduction_bloc bloc ^ jumpDebut ^ etiquetteFin ^ "\n" in
    (0, phrase)
  | AstType.Empty -> (0, "")
      
(* traduction_bloc : AstPlacement.bloc -> string *)
(* Paramètre li : liste d'instructions à analyser *)
(* Renvoi le code TAM correspondant au bloc *)
and traduction_bloc li =
  (* Concaténation des instructions puis pop (0) tailleVarLocales *)
  let result = List.fold_left(fun acc elt -> 
    let returnInstruction = traduction_instruction elt
    in (fst acc + fst returnInstruction, snd acc ^ snd returnInstruction)
    ) (0, "") li
  in
  snd result ^ "POP (0) " ^ string_of_int (fst result) ^ "\n"


(* traduction_fonction : AstPlacement.fonction -> string *)
(* Paramètre : la fonction à analyser *)
(* Renvoi le code associé à la fonction *)
let traduction_fonction (AstPlacement.Fonction(name,_,linstr,exprRetour))  =
  (* 
  Etiquette (nom fonction)
  Analyse de la liste d'instruction
  Analyse expression
  POP (taille type retour) taille var locale
  RETURN (taille type retour) taille paramètre  
  *)
  match info_ast_to_info name with
  | InfoFun(name, rtyp, lparamTyp) ->
    let etiquetteFun = name ^ "\n" in
    let instr = List.fold_left(fun acc elt -> let tradInst = traduction_instruction elt in (fst acc + fst tradInst, snd acc ^ snd tradInst)) (0, "") linstr in
    let tradExpr = traduction_expression exprRetour in
    let tailleParam = List.fold_left(fun acc elt -> acc + getTaille elt) 0 lparamTyp in
    let pop = "POP (" ^ string_of_int (getTaille rtyp) ^ ") " ^ string_of_int (fst instr) ^ "\n" in
    let return = "RETURN (" ^ string_of_int (getTaille rtyp) ^ ") " ^ string_of_int tailleParam ^ "\n" in
    etiquetteFun ^ snd instr ^ tradExpr ^ pop ^ return ^ "\n"
  | _ -> failwith "Internal error"

(* analyser : AstType.ast -> AstPlacement.ast *)
(* Paramètre : le programme à analyser *)
(* Défini le placement mémoire des variables et fonctions du programmes *)
let analyser (AstPlacement.Programme (fonctions,prog)) =
  let nf = List.fold_left(fun acc elt-> acc ^ traduction_fonction elt) "" fonctions in 
  let nb = traduction_bloc prog in
  let entete = getEntete() in
  entete ^ nf ^ "main\n" ^ nb ^ "HALT"(* Concat fonctions, concat prog, concat halt *)

end

