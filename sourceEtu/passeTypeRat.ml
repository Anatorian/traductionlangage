(* Module de la passe de gestion des types *)
module PasseTypeRat : Passe.Passe with type t1 = Ast.AstTds.programme and type t2 = Ast.AstType.programme =
struct

  open Tds
  open Exceptions
  open Ast
  open AstType
  open Type

  type t1 = Ast.AstTds.programme
  type t2 = Ast.AstType.programme


(* analyse_type_expression : AstTds.expression -> AstType.expression *)
(* Paramètre e : l'expression à analyser *)
(* Vérifie la bonne utilisation des types et transforme l'expression
en une expression de type AstType.expression*)
(* Erreur si mauvaise utilisation des types *)
let rec analyse_type_expression e = 
  match e with
  | AstTds.AppelFonction (info_ast, expression_list) -> 
    let nExpr = List.map(fun x -> analyse_type_expression x) expression_list in
    let nExprType = List.map(fun x -> snd x) nExpr in
      begin
        match info_ast_to_info info_ast with
        | InfoFun(_, _, tList) -> if est_compatible_list tList nExprType then
          let nExprList = List.map(fun x -> fst x) nExpr in
          begin
              match info_ast_to_info info_ast with
              | InfoFun(_, t, _) -> (AstType.AppelFonction(info_ast, nExprList), t)
              | _ -> failwith "Internal error"
            end
          else
            raise (TypesParametresInattendus(nExprType, tList))
        | _ -> failwith "Internal Error"
      end
  | AstTds.Rationnel(expression1, expression2) ->
    let (nExpr1, nType1) = analyse_type_expression expression1 in
    let (nExpr2, nType2) = analyse_type_expression expression2 in
    if est_compatible Int nType1 then
      if est_compatible Int nType2 then
        (AstType.Rationnel(nExpr1, nExpr2), Rat)
      else
        raise (TypeInattendu (nType2, Int))
    else
      raise (TypeInattendu (nType1, Int))
  | AstTds.Numerateur(expression)->
    let (nExpression, nType) = analyse_type_expression expression in
    begin
      if est_compatible Rat nType then
        (AstType.Numerateur(nExpression), Int)
      else
        raise (TypeInattendu  (nType, Rat))
    end
  | AstTds.Denominateur(expression)->
    let (nExpression, nType) = analyse_type_expression expression in
    begin
      if est_compatible Rat nType then
        (AstType.Denominateur(nExpression), Int)
      else
        raise (TypeInattendu (nType, Rat))
    end
  | AstTds.Ident(info_ast) ->
    begin
      match info_ast_to_info info_ast with
      | InfoVar(_,t,_,_) -> (AstType.Ident(info_ast), t)
      | InfoConst _ -> (AstType.Ident(info_ast), Int)
      | _ -> failwith "Internal error"
    end
  | AstTds.Binaire(operateur, expr1, expr2) ->
    let (nExpr1, nType1) = analyse_type_expression expr1 in
    let (nExpr2, nType2) = analyse_type_expression expr2 in
    begin
      match (operateur, nType1, nType2) with
      | (Plus, Int, Int) -> (AstType.Binaire(PlusInt, nExpr1, nExpr2), Int)
      | (Plus, Rat, Rat) -> (AstType.Binaire(PlusRat, nExpr1, nExpr2), Rat)
      | (Mult, Int, Int) -> (AstType.Binaire(MultInt, nExpr1, nExpr2), Int)
      | (Mult, Rat, Rat) -> (AstType.Binaire(MultRat, nExpr1, nExpr2), Rat)
      | (Equ, Int, Int) -> (AstType.Binaire(EquInt, nExpr1, nExpr2), Bool)
      | (Equ, Bool, Bool) -> (AstType.Binaire(EquBool, nExpr1, nExpr2), Bool)
      | (Inf, Int, Int) -> (AstType.Binaire(Inf, nExpr1, nExpr2), Bool)
      | _ -> raise (TypeBinaireInattendu(operateur, nType1, nType2))
    end
  | True -> (AstType.True, Bool)
  | False -> (AstType.False, Bool)
  | Entier(entier) -> (AstType.Entier(entier), Int)

(* analyse_type_instruction : AstTds.instruction -> AstType.instruction *)
(* Paramètre i : l'instruction à analyser *)
(* Vérifie la bonne utilisation des types et tranforme l'instruction
en une instruction de type AstType.instruction *)
(* Erreur si mauvaise utilisation des types *)
let rec analyse_type_instruction  i =
  match i with
  | AstTds.Declaration(typ, expression, info_ast) -> 
      let (nExpression, nType) = analyse_type_expression expression in
        if est_compatible typ nType then
          begin
            modifier_type_info typ info_ast;
            AstType.Declaration (nExpression, info_ast)
          end
        else
          raise (TypeInattendu (nType, typ))
  | AstTds.Affectation(expression, info_ast) ->
    let (nExpression, nType) = analyse_type_expression expression in
    begin  
      match info_ast_to_info info_ast with
      | InfoVar(_,typ, _,_) ->
        if est_compatible typ nType then
          AstType.Affectation (nExpression, info_ast)
        else
          raise (TypeInattendu (nType, typ))
      | _ -> failwith "Internal Error"
    end
  | AstTds.Affichage(expression) ->
    let (nExpression, nType) = analyse_type_expression expression in
    begin  
      match nType with
      | Int -> AstType.AffichageInt nExpression
      | Bool -> AstType.AffichageBool nExpression
      | Rat -> AstType.AffichageRat nExpression
      | _ -> failwith "Internal error"
    end
  | AstTds.Conditionnelle(expression, bloc1, bloc2) ->
    let (nExpression, nType) = analyse_type_expression expression in
      if est_compatible Bool nType then
        AstType.Conditionnelle (nExpression, analyse_type_bloc bloc1, analyse_type_bloc bloc2)
      else
        raise (TypeInattendu (nType, Bool))
  | AstTds.TantQue(expression, bloc)->
    let (nExpression, nType) = analyse_type_expression expression in
      if est_compatible Bool nType then
        AstType.TantQue(nExpression, analyse_type_bloc bloc)
      else
        raise (TypeInattendu (nType, Bool))
  | AstTds.Empty -> AstType.Empty

      
(* analyse_type_bloc : AstTds.bloc -> AstType.bloc *)
(* Paramètre li : liste d'instructions à analyser *)
(* Vérifie la bonne utilisation des types et tranforme le bloc
en un bloc de type AstType.bloc *)
(* Erreur si mauvaise utilisation des types *)
and analyse_type_bloc  li =
  List.map (analyse_type_instruction) li


(* analyse_tds_fonction : AstTds.fonction -> AstType.fonction *)
(* Paramètre : la fonction à analyser *)
(* Vérifie la bonne utilisation des types et tranforme la fonction
en une fonction de type AstType.fonction *)
(* Erreur si mauvaise utilisation des types *)
let analyse_type_fonction (AstTds.Fonction(t,n,lp,li,e))  =
  let instructionList = List.map(fun x -> analyse_type_instruction x) li in
  let (nExpr, nType) = analyse_type_expression e in
  if est_compatible t nType then
    AstType.Fonction(n, List.map(fun x -> snd x) lp, instructionList, nExpr)
  else
    raise (TypeInattendu(nType, t))
    


(* analyser : AstTds.ast -> AstType.ast *)
(* Paramètre : le programme à analyser *)
(* Vérifie la bonne utilisation des types et tranforme le programme
en un programme de type AstType.ast *)
(* Erreur si mauvaise utilisation des types *)
let analyser (AstTds.Programme (fonctions,prog)) =
  let nf = List.map (analyse_type_fonction) fonctions in 
  let nb = analyse_type_bloc prog in
  AstType.Programme (nf,nb)

end
